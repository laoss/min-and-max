package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var tests = []struct {
	input  [5]int
	expMin int
	expMax int
}{
	{[5]int{-9, 23, 65, 0, 12}, 26, 100},
	{[5]int{-9, -23, -65, -1, -12}, -109, -45},
	{[5]int{0, 0, 0, 1, 0}, 0, 1},
	{[5]int{20, 128, 8, 45, 55}, 128, 248},
	{[5]int{19, -9, -10, -7, -3}, -29, 0},
}

func TestMinSum(t *testing.T) {
	for _, e := range tests {
		assert.Equal(t, e.expMin, minSum(e.input))
	}
}

func TestMaxSum(t *testing.T) {
	for _, e := range tests {
		assert.Equal(t, e.expMax, maxSum(e.input))
	}
}
