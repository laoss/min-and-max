package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello")
	arr := [5]int{2, 34, 8, 4, 5}

	x, y := minSum(arr), maxSum(arr)

	fmt.Printf("minimum: %d, maximum: %d\n", x, y)
}

// MinSum - Calculates the sum of the 4 minimum values of an array of length 5
func minSum(a [5]int) (sum int) {
	maxV := a[0]
	for _, v := range a {
		if v > maxV {
			maxV = v
		}
		sum += v
	}

	return sum - maxV
}

// MaxSum - Calculates the sum of the 4 maximum values of an array of length 5
func maxSum(a [5]int) (sum int) {
	minV := a[0]
	for _, v := range a {
		if v < minV {
			minV = v
		}
		sum += v
	}

	return sum - minV
}
